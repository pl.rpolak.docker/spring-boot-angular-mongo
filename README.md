# spring-boot-angular

This project is an example of how to create:
- spring boot app (backend)
- angular app (frontend)
- mongo db (database)
as a single docker container combined from projects